/*
  Main "app" module
 */
angular.module('mainModule', [
  'translatedUrls'
]).run([
  'uiRouterProxy',
  'translationsCache',
  function(uiRouterProxy, translationsCache) {
    uiRouterProxy.setOtherwise('/');
    uiRouterProxy.addState('main', {
      url: '/',
      template: '<ul>' +
      '<li><a ui-sref="module1">module1</a></li>' +
      '<li><a ui-sref="module2">module2</a></li>' +
      '<li><a ui-sref="deferredModule">deferredModule (Its URLs will be ' +
      'available after 1 second after the application launched)</a></li>' +
      '</ul>',
      resolve: {
        // Fetch the translations for URLs that used in template:
        // *) module1
        // *) module2
        // *) deferredModule
        loadTranslatedUrls: function() {
          return translationsCache.fetch([
            '/module1',
            '/module2',
            '/deferredModule'
          ]);
        }
      }
    });
  }
]);
