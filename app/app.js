angular.module('app', [
  'mockBackend',
  
  'translatedUrls',

  'mainModule',
  'module1',
  'module2',
  'deferredModule'
]).run([
  'uiRouterProxy',
  '$timeout',
  'urlTranslationHooks',
  'urlTranslator',
  function(uiRouterProxy, $timeout, urlTranslationHooks, urlTranslator) {
    /*
      We need connect 'urlTranslator' from 'translatedUrls.app' module with 
      'urlTranslationHooks' from 'translatedUrls.core'.
      On the one hand this can be done automatically.
      On the other hand, the way, that will be used in order to translate 
      URLs, depends on the implementation of a particular application.
      This is the reason why there are 'translatedUrls.core' and 'translatedUrls.app' 
      modules.
    */
    urlTranslationHooks.toExternalUrl(urlTranslator.toExternal);
    urlTranslationHooks.toInternalUrl(urlTranslator.toInternal);
    
    // TODO: we need fetch internal URL for current translated URL before we start routing
    
    /*
      Allow other modules to register the states.
      Also see 'deferredModule' as an example of the deferred registration of the modules
    */
    $timeout(uiRouterProxy.start);
  }
]);
