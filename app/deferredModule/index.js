/*
  Example of the deferred registration of the states. 
 */
angular.module('deferredModule', [
  'translatedUrls'
]).run([
  'uiRouterProxy',
  '$timeout',
  function(uiRouterProxy, $timeout) {
    var states = {
      'deferredModule': {
        url: '/deferredModule',
        template: '<div>' +
        '<h4>deferredModule</h4>' +
        '<ul>' +
        '<li><a ui-sref="deferredModule.state1">deferredModule / state1</a></li>' +
        '<li><a ui-sref="deferredModule.state2">deferredModule / state2</a></li>' +
        '</ul>' +
        '<div ui-view></div>' +
        '</div>'
      },
      'deferredModule.state1': {
        url: '/state1',
        template: '<span>deferredModule / state 1</span>'
      },
      'deferredModule.state2': {
        url: '/state2',
        template: '<span>deferredModule / state 2</span>'
      }
    };

    $timeout(function() {
      angular.forEach(states, function(stateConfig, stateName) {
        uiRouterProxy.addState(stateName, stateConfig);
      }, 1000);
    });
  }
]);
