/*
  Some other module
 */
angular.module('module1', [
  'translatedUrls'
]).run([
  'uiRouterProxy',
  'translationsCache',
  function(uiRouterProxy, translationsCache) {
    var states = {
      'module1': {
        url: '/module1',
        template: '<div>' +
        '<h4>Module 1</h4>' +
        '<ul>' +
        '<li><a ui-sref="module1.state1">module1 / state1</a></li>' +
        '<li><a ui-sref="module1.state2">module1 / state2</a></li>' +
        '</ul>' +
        '<div ui-view></div>' +
        '</div>',
        resolve: {
          // Fetch the translations for URLs that used in template:
          // *) module1.state1
          // *) module1.state2
          loadTranslatedUrls: function() {
            return translationsCache.fetch([
              '/module1/state1',
              '/module1/state2'
            ]);
          }
        }
      },
      'module1.state1': {
        url: '/state1',
        template: '<span>module1 / state 1</span>'
      },
      'module1.state2': {
        url: '/state2',
        template: '<span>module1 / state 2</span>'
      }
    };

    angular.forEach(states, function(stateConfig, stateName) {
      uiRouterProxy.addState(stateName, stateConfig);
    });
  }
]);
