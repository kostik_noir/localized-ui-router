/**
 * Wrapper around ui.router which allow:
 * a) register the states
 * b) call $urlRouterProvider.otherwise
 * during the 'run' phase of the angular application.
 * 
 * Also it block any transitions between states until you call its 'start' method.
 */
angular.module('translatedUrls.core')
  .provider('uiRouterProxy', [
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
      this.$get = [
        '$rootScope',
        function($rootScope) {
          var otherwiseRule = null;
          var states = [];
          var started = false;
          var api = {};

          var stateChangeStartHandler = $rootScope.$on('$stateChangeStart', function(event) {
            event.preventDefault();
          });

          /**
           * Register state.
           * @param {string} stateName
           * @param {Object} stateConfig
           */
          api.addState = function(stateName, stateConfig) {
            if (started) {
              $stateProvider.state(stateName, stateConfig);
            } else {
              states.push({
                name: stateName,
                config: stateConfig
              });
            }
          };

          /**
           * Set 'otherwise' rule for $urlRouterProvider.otherwise
           * @param {string|function} rule
           */
          api.setOtherwise = function(rule) {
            otherwiseRule = rule;
          };

          /**
           * Start routing
           */
          api.start = function() {
            if (started) {
              return;
            }
            started = true;

            var state;
            while (states.length) {
              state = states.pop();
              $stateProvider.state(state.name, state.config);
            }

            if (otherwiseRule !== null) {
              $urlRouterProvider.otherwise(otherwiseRule);
            }

            stateChangeStartHandler();
            $rootScope.$emit('$locationChangeSuccess');
          };

          return api;
        }
      ];
    }
  ]);
