/**
 * This module contains core functionality that required in order to implement 
 * 'translated URLs' feature.
 */
angular.module('translatedUrls.core', [
  'ui.router'
]);
