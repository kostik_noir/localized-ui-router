/**
 * There are 2 types of URLs:
 * a) 'internal' - URLs that are used during registration of the states. 
 * For example when you register the state like this:
 *   $stateProvider.state('my-state', {
 *     url: '/my-state-url',
 *     template: '<h4>My state</h4>'
 *   });
 * the '/my-state-url' is 'internal' URL
 * b) 'external' - translated URLs that are visible in the address bar 
 * of the browser. You can have a lot of 'external' URLs (i.e. translated 
 * URLs) for single 'internal' URL.
 * 
 * This service allows you to add a translators that will be used by 
 * the UrlMatcher instances in order to translate URLs from 'internal' 
 * URLs to 'external' URLs and conversely.
 */
angular.module('translatedUrls.core').provider('urlTranslationHooks', [
  '$stateProvider',
  function($stateProvider) {
    var translators = {
      toExternal: [],
      toInternal: []
    };

    function translatePath(path, fns) {
      var count = fns.length;
      for (var i = 0; i < count; i++) {
        path = fns[i](path);
      }
      return path;
    }

    $stateProvider.decorator('url', function(state, superFn) {
      var urlMatcher = superFn(state);

      var originalExecFn = urlMatcher.exec;
      urlMatcher.exec = function(path, searchParams) {
        path = translatePath(path, translators.toInternal);
        return originalExecFn.call(urlMatcher, path, searchParams);
      };

      var originalFormatFn = urlMatcher.format;
      urlMatcher.format = function(values) {
        var path = originalFormatFn.call(urlMatcher, values);
        path = translatePath(path, translators.toExternal);
        return path;
      };

      return urlMatcher;
    });

    this.$get = function() {
      var api = {};

      /**
       * Translate function
       * @callback translateFunction
       * @param {string} url
       * @returns {string}
       */

      /**
       * Add 'from internal to external URL' translator
       * @param {translateFunction} translateFn
       */
      api.toExternalUrl = function(translateFn) {
        translators.toExternal.push(translateFn);
      };

      /**
       * Add 'from external to internal URL' translator
       * @param {translateFunction} translateFn
       */
      api.toInternalUrl = function(translateFn) {
        translators.toInternal.push(translateFn);
      };

      return api;
    };
  }
]);
