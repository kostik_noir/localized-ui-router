/**
 * This is a repository for the translations.
 */
angular.module('translatedUrls.app').service('translationsCache', [
  '$http',
  '$q',
  function($http, $q) {
    var GET_TRANSLATION_URL = '/get-translation';

    var cache = {
      byInternal: {},
      byExternal: {}
    };

    var api = {};

    /**
     * Add the translation for 'internal' URL
     * @param {string} internalUrl
     * @param {string} externalUrl
     */
    api.add = function(internalUrl, externalUrl) {
      cache.byInternal[internalUrl] = externalUrl;
      cache.byExternal[externalUrl] = internalUrl;
    };

    /**
     * Load translation for 'internal' URL
     * @param {string|Array} internalUrls
     * @returns {Promise}
     */
    api.fetch = function(internalUrls) {
      if (!angular.isArray(internalUrls)) {
        internalUrls = [internalUrls];
      }

      var unknownUrls = [];
      angular.forEach(internalUrls, function(url) {
        if (typeof cache.byInternal[url] === 'undefined') {
          unknownUrls.push(url);
        }
      });

      if (unknownUrls.length === 0) {
        return $q.when(true);
      }

      // Let's suppose that there is a backend service that accept 
      // an array of internal urls and returns the translations
      var cfg = {
        url: GET_TRANSLATION_URL,
        method: 'POST',
        data: unknownUrls
      };

      return $http(cfg).then(function(response) {
        // Let's suppose that response is a hash that use internal urls as 
        // a key and external url as a value
        angular.forEach(response.data, function(externalUrl, internalUrl) {
          api.add(internalUrl, externalUrl);
        });
        return $q.when(true);
      })
    };

    /**
     * Return the translation for 'internal' URL
     * @param {string} internalUrl
     * @returns {string|null}
     */
    api.getExternalUrl = function(internalUrl) {
      return cache.byInternal[internalUrl] || null;
    };

    /**
     * Return 'internal' URL for 'external' URL
     * @param {string} externalUrl
     * @returns {string|null}
     */
    api.getInternalUrl = function(externalUrl) {
      return cache.byExternal[externalUrl] || null;
    };

    return api;
  }
]);
