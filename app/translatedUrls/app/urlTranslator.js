/**
 * The purpose of this service is to translate URLs from 'internal' URLs 
 * to 'external' URLs and conversely.
 * In the current implementation it comprises two simple methods that 
 * use the values from 'translationsCache'.
 * Theoretically it is possible to add other methods that will use more complex rules and logic.
 */
angular.module('translatedUrls.app').service('urlTranslator', [
  'translationsCache',
  function(translationsCache) {
    var api = {};

    /**
     * Translate 'internal' url to 'external' url
     * @param {string} internalUrl
     * @returns {string}
     */
    api.toExternal = function(internalUrl) {
      var externalUrl = translationsCache.getExternalUrl(internalUrl);
      if (externalUrl === null) {
        return internalUrl;
      }
      return externalUrl;
    };

    /**
     * Translate 'external' url to 'internal' url
     * @param {string} externalUrl
     * @returns {string}
     */
    api.toInternal = function(externalUrl) {
      var internalUrl = translationsCache.getInternalUrl(externalUrl);
      if (internalUrl === null) {
        return externalUrl;
      }
      return internalUrl;
    };

    return api;
  }
]);
