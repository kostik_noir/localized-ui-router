/**
 * This module contains functionality that functionality that is specific to 
 * 'this' application
 */
angular.module('translatedUrls.app', []);
