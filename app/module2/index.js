/*
  Some other module
 */
angular.module('module2', [
  'translatedUrls'
]).run([
  'uiRouterProxy',
  function(uiRouterProxy) {
    var states = {
      'module2': {
        url: '/module2',
        template: '<div>' +
        '<h4>Module 1</h4>' +
        '<ul>' +
        '<li><a ui-sref="module2.state1">module2 / state1</a></li>' +
        '<li><a ui-sref="module2.state2">module2 / state2</a></li>' +
        '</ul>' +
        '<div ui-view></div>' +
        '</div>'
      },
      'module2.state1': {
        url: '/state1',
        template: '<span>module2 / state 1</span>'
      },
      'module2.state2': {
        url: '/state2',
        template: '<span>module2 / state 2</span>'
      }
    };
    
    angular.forEach(states, function(stateConfig, stateName) {
      uiRouterProxy.addState(stateName, stateConfig);
    });
  }
]);
