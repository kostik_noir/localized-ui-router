/**
 * Mock HTTP requests to backend
 */
angular.module('mockBackend', [
  'ngMockE2E'
]).run([
  '$httpBackend',
  function($httpBackend) {
    var translations = {
      '/deferredModule': '/translated-deferred-module',
      '/deferredModule/state1': '/translated-deferred-module-state-1',
      '/deferredModule/state2': '/translated-deferred-module-state-2',
      
      '/module1': '/translated-module-1',
      '/module1/state1': '/translated-module-1-state-1',
      '/module1/state2': '/translated-module-1-state-2'
    };

    $httpBackend.whenPOST('/get-translation').respond(function(method, url, data) {
      var resultUrls = {};
      try {
        data = JSON.parse(data);

        angular.forEach(data, function(internalUrl) {
          resultUrls[internalUrl] = translations[internalUrl] || internalUrl;
        });
      }
      catch (e) {
      }
      return [200, resultUrls];
    });
  }
]);
