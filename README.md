# Short description

This is a draft. There are no tests, build systems etc. The main purpose of this repository is to clarify details.  
Almost all files contains comments.

-------

Modules in directories

+ app/deferredModule
+ app/module1
+ app/module2

are examples of some modules.  
Module `deferredModule` is the example of deferred registration of the states.

------

Module `mainModule` (see app/mainModule directory) is a _main_ module.

------

Module `mockBackend` (see app/mockBackend directory) used in order to mock HTTP requests to backend.

------

Module `translatedUrls` (see app/translatedUrls) is the thing for which everything was started
